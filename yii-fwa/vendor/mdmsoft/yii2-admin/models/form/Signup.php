<?php
namespace mdm\admin\models\form;

use mdm\admin\components\UserStatus;
use mdm\admin\components\StatusKaryawan;
use mdm\admin\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\models\Unit1;
use app\models\Unit2;
use app\models\Kota;

/**
 * Signup form
 */
class Signup extends Model
{
    public $nik;
    public $nama;
    public $unit1_id;
    public $unit2_id;
    public $unit2;
    public $email;
    public $kota_id;
    public $status_karyawan;
    public $password;
    public $retypePassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $class = Yii::$app->getUser()->identityClass ? : 'mdm\admin\models\User';
        return [
            ['nik', 'filter', 'filter' => 'trim'],
            ['nik', 'required'],
            ['nik', 'unique', 'targetClass' => $class, 'message' => 'This nik has already been taken.'],
            ['nik', 'string', 'min' => 5, 'max' => 255],

            [['unit1_id', 'unit2_id', 'status_karyawan'], 'integer'],
            [['nama', 'unit1_id', 'unit2_id'], 'required'],
            [['nama'], 'string', 'max' => 255],
            [['unit2_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit2::className(), 'targetAttribute' => ['unit2_id' => 'id']],
            [['unit1_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit1::className(), 'targetAttribute' => ['unit1_id' => 'id']],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => $class, 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['retypePassword', 'required'],
            ['retypePassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $class = Yii::$app->getUser()->identityClass ? : 'mdm\admin\models\User';
            $user = new $class();
            $user->nik = $this->nik;
            $user->nama = $this->nama;
            $user->unit1_id = $this->unit1_id;
            $user->unit2_id = $this->unit2_id;
            $user->email = $this->email;
            $user->status = ArrayHelper::getValue(Yii::$app->params, 'user.defaultStatus', UserStatus::ACTIVE);
            $user->status_karyawan = ArrayHelper::getValue(Yii::$app->params, 'user.defaultStatusK', StatusKaryawan::OUTSOURCE);
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
