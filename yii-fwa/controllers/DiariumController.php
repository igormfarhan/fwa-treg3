<?php

namespace app\controllers;

use Yii;
use app\models\Diarium;
use app\models\DiariumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Unit2;
use app\models\Lokasi;
use app\models\Kondisi;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;


/**
 * DiariumController implements the CRUD actions for Diarium model.
 */
class DiariumController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            // 'access' => [
            //     'class' => AccessControl::className(),
            //     'rules' => [
            //         [
            //             'actions' => ['index', 'rekap'],
            //             'allow' => true,
            //             // 'roles' => ['@'],
            //         ],
            //     ],
            // ],
        ];
    }

    /**
     * Lists all Diarium models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DiariumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $unit2 = Unit2::find()->all();
        $unit2 = ArrayHelper::map($unit2,'id','unit2');

        $lokasi = Lokasi::find()->all();
        $lokasi = ArrayHelper::map($lokasi,'id','lokasi');

        $kondisi = Kondisi::find()->all();
        $kondisi = ArrayHelper::map($kondisi,'id','kondisi');

        $searchModel = new DiariumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'unit2' => $unit2,
            'lokasi' => $lokasi,
            'kondisi' => $kondisi,
        ]);
    }

    /**
     * Displays a single Diarium model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Diarium model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Diarium();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Diarium model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Diarium model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Diarium model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Diarium the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Diarium::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRekap()
    {       
        $model = new Diarium();       

        if ($model->load(Yii::$app->request->post())) {

            $date = $model->today;
            
        }  else {
            $date = date('Y-m-d');
            
        }

        // $query = (new Query())
        //          ->select('u.id,u.nik')
        //          ->from('laporan l')
        //          ->rightJoin('user u', 'l.user_id=u.id')
        //          ->where(['l.user_id'=> null]);
        $subquery = Diarium::find()->select('nik_id')->where(['like','submit_date',$date]);

        $query = Diarium::find()->select(['u.id AS id','u.nik AS nik'])
                        ->from('laporan l')
                        ->rightJoin('user u', ['u.nik'=>$subquery])
                        ->where(['l.nik_id'=> null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        
        $searchModel = new DiariumSearch();
        // $dataProvider = $searchModel->search2(Yii::$app->request->queryParams,$query);

        return $this->render('rekap',[
            'model' => $model,
            'date' => $date,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);   
                                  
    }
}
