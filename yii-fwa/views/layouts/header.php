<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <?php
    NavBar::begin();
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            ['label' => 'Diarium', 'url' => ['diarium/']],
            ['label' => 'Rekap Diarium', 'url' => ['/diarium/rekap']],            
        ],
    ]);
    echo Nav::widget([
      'options' => ['class' => 'navbar-nav navbar-right'],
      'items' => [
          Yii::$app->user->isGuest ? (
              ['label' => 'Login', 'url' => ['/site/login']]
          ) : (
              '<li>'
              . Html::a('Logout ('.Yii::$app->User->identity->nama .')', 
              ['/site/logout'],['data-method' => 'post'])
              . '</li>'
          )
      ],
  ]);
    NavBar::end();
    ?>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
