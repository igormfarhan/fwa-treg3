<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><?= Html::a('Diarium', ['diarium/'])?></li> 
            <li><?= Html::a('Rekap Diarium', ['diarium/rekap'])?></li> 
               
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                <li class="logout" style="margin-right: 50px">
                    <?= Html::a('Logout ('.Yii::$app->User->identity->nama .')', 
                    ['/site/logout'],['data-method' => 'post']) ?>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li>                    
                </li>
            </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
