<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Auth Key Generator';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        for ($x = 0; $x <= 50; $x++) {
            echo Yii::$app->security->generateRandomString() ."<br>";
    }
    ?>

</div>
